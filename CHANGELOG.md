# Changelog of json2html

## V0.5.0 (2024-12-08)

- Json2htmlAttr is missing state undefined
- update demo to Angular 19

## V0.4.2 (2023-05-31)

- Fix for not crash with old Webkit

## V0.4.1 (2023-02-18)

- Fix ` protection

## V0.4.0 (2023-02-18)

- add `Json2Js` to tranform json to js style

## V0.3.0 (2023-02-17)

- rename lib `json2html-lib` to `@ikilote/json2html`
- fix quote render
- add javascript render in demo

## V0.2.0 (2022-05-30)

- add option `wrapAttrNumber`
- add options `prettier` on `attrPosition`
- add options `maxLength` for text and `attrPosition` (`inline *`)
- fix `inline` render
- update demo

## V0.1.0 (2022-02-10)

- with Ivy partial compilation mode

## V0.0.6 (2020-10-21)

- add option `optionalEndTagsFix`
- add options `inline` and `autoclose` on json node
- fix minor bugs

## V0.0.5 (2020-10-18)

- fix numeric options
- add demo

## V0.0.4 (2020-10-15)

- add `xml` type of render
- code comments
- fix minor bugs

## V0.0.3 (2020-10-14)

- change to accept lists of elements or not

## V0.0.2 (2020-10-14)

- fix attributs render

## V0.0.1 (2020-10-14)

- initial release
